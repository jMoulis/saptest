sap.ui.define([
	"./BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("exo.exo.controller.DetailObjectNotFound", {});
});
